import React from "react";
import AdminControlButton from "./AdminControl";
import ChangePasswordButton from "./ChangePasswordButton";
import UserInfoListItem from "./UserInfoListItem";
import { useState, useEffect } from "react";
import { createAPIEndpoint } from "../../../API";
import useStateContext from "../../Login/Hooks/useStateContext";

export default function UserInfo() {
  // Use states for each field of Profile
  const { context } = useStateContext();
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const [postalCode, setPostalCode] = useState("");
  const [city, setCity] = useState("");
  const [country, setCountry] = useState("");
  const [weight, setWeight] = useState("");
  const [height, setHeight] = useState("");
  const [medicalConditions, setMedicalConditions] = useState("");
  const [disabilities, setDisabilities] = useState("");
  const [role, setRole] = useState();
  const [planId, setPlanId] = useState();
  const [isAdmin, setIsAdmin] = useState();

  useEffect(() => {
    // Get current account on page load
    getAccount();
  }, []);

  const passFirstName = (firstName) => {
    setFirstName(firstName);
  };

  const passLastName = (lastName) => {
    setLastName(lastName);
  };

  const passEmail = (email) => {
    setEmail(email);
  };

  const passAddress = (address) => {
    setAddress(address);
  };

  const passPostalCode = (postalCode) => {
    setPostalCode(postalCode);
  };

  const passCity = (city) => {
    setCity(city);
  };

  const passCountry = (country) => {
    setCountry(country);
  };

  const passWeight = (weight) => {
    setWeight(weight);
  };

  const passHeight = (height) => {
    setHeight(height);
  };

  const passMedicalConditions = (medicalConditions) => {
    setMedicalConditions(medicalConditions);
  };

  // On click, set new information
  const handleUpdateClick = async (e) => {
    e.preventDefault();
    setAccount();
  };
  // Context shared with app
  var userId = context.userId;
  // Get current user
  const getUserById = async () => {
    const user = await createAPIEndpoint("User").getById(userId);
    return user;
  };

  var userPassword = "";
  // Get current password and set same for no change
  const getUserPassword = async () => {
    var user = await getUserById();
    userPassword = user.data.password;
  };
  getUserPassword();

  const getAccount = async () => {
    // Call and set all info to profile
    var account = await createAPIEndpoint("Account").getById(userId);
    var user = await createAPIEndpoint("User").getById(userId);
    var address = await createAPIEndpoint("Address").getById(userId);
    setFirstName(Object.values(user.data)[1]);
    setLastName(Object.values(user.data)[2]);
    setEmail(Object.values(user.data)[3]);
    setIsAdmin(Object.values(user.data)[5]);
    setAddress(Object.values(address.data)[1]);
    setPostalCode(Object.values(address.data)[2]);
    setCity(Object.values(address.data)[3]);
    setCountry(Object.values(address.data)[4]);
    setWeight(Object.values(account.data)[1]);
    setHeight(Object.values(account.data)[2]);
    setMedicalConditions(Object.values(account.data)[3]);
    setDisabilities(Object.values(account.data)[4]);
    setPlanId(Object.values(account.data)[8]);

    // Set admin/member/contributor text depending on role
    var isAdmin = Object.values(user.data)[5];
    var isContributor = Object.values(user.data)[6];
    var temp = "";
    if (!isAdmin && !isContributor) {
      temp = "Member";
    } else if (isAdmin) {
      temp = "Administrator";
    } else {
      temp = "Contributor";
    }
    setRole(temp);
  };

  // Set information for user to see
  const setAccount = async () => {
    var userId = context.userId;
    const accountPayload = {
      Id: userId,
      Weight: weight,
      Height: height,
      Medical_Conditions: medicalConditions,
      Disabilities: disabilities,
      UserID: userId,
      AddressID: userId,
      StatsID: userId,
      // If no plan, sett null
      PlanID: context.planId ? context.planId : null,
    };
    const userPayload = {
      Id: userId,
      First_Name: firstName,
      Last_Name: lastName,
      Email: email,
      Password: userPassword,
      Is_Contributor: false,
      Is_Admin: isAdmin,
    };
    const addressPayload = {
      Id: userId,
      Address_1: address,
      Postal_Code: postalCode,
      City: city,
      Country: country,
    };
    await createAPIEndpoint("Account").put(userId, accountPayload);
    await createAPIEndpoint("User").put(userId, userPayload);
    await createAPIEndpoint("Address").put(userId, addressPayload);
    alert("User details updated");
  };

  return (
    <div className="user-info-container">
      <div className="user-info-header">
        <h2 className="first-name">{firstName ? firstName : "First Name"}</h2>
        <h2 className="last-name">{lastName ? lastName : "Last Name"}</h2>
        <div className="name-container"></div>
        <h2 className="role-name">{role}</h2>
      </div>
      <div className="info-box">
        <form className="profile-form">
          <div className="user-info-title-container">
            <h3 className="user-info-title">Account details</h3>
          </div>
          <fieldset className="profile-fieldset">
            <label htmlFor="profile"></label>
            <div className="innerProfileBox">
              <UserInfoListItem
                placeholder={firstName ? firstName : "First Name"}
                passValue={passFirstName}
              />
              <UserInfoListItem
                placeholder={lastName ? lastName : "Last Name"}
                passValue={passLastName}
              />
              <UserInfoListItem
                placeholder={email ? email : "Email"}
                passValue={passEmail}
              />
              <UserInfoListItem
                placeholder={address ? address : "Address"}
                passValue={passAddress}
              />
              <UserInfoListItem
                placeholder={postalCode ? postalCode : "Postal Code"}
                passValue={passPostalCode}
              />
              <UserInfoListItem
                placeholder={city ? city : "City"}
                passValue={passCity}
              />
              <UserInfoListItem
                placeholder={country ? country : "Country"}
                passValue={passCountry}
              />
              <UserInfoListItem
                placeholder={height ? height + " cm" : "Height"}
                passValue={passHeight}
              />
              <UserInfoListItem
                placeholder={weight ? weight + " kg" : "Weight"}
                passValue={passWeight}
              />
              <UserInfoListItem
                placeholder={
                  medicalConditions ? medicalConditions : "Medical Conditions"
                }
                passValue={passMedicalConditions}
              />
              {/* <UserInfoListItem
                placeholder={disabilities}
                passValue={passDisabilities}
              /> */}
            </div>
          </fieldset>
          <div className="button-container">
            <button
              type="submit"
              className="update-profile-button"
              onClick={handleUpdateClick}
            >
              Update Profile
            </button>
            <ChangePasswordButton />
            <AdminControlButton />
          </div>
        </form>
      </div>
    </div>
  );
}
