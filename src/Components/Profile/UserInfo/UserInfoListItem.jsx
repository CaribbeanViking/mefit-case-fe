import React from "react";
import { useState } from "react";

export default function UserInfoListItem(props) {
  const [field, setField] = useState("");

  const handleInputChange = (e) => {
    setField(e.target.value);
    props.passValue(e.target.value);
  };

  return (
    <div className="user-info-list-item-container">
      <input
        autoComplete="off"
        type="text"
        className="user-info-list-item-input"
        placeholder={props.placeholder}
        onChange={handleInputChange}
        value={field}
      />
    </div>
  );
}
