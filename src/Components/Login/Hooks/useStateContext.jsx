import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { createContext } from "react";
import { useContext } from "react";

export const stateContext = createContext();

const getNewContext = () => {
  if (localStorage.getItem("context") === null)
    localStorage.setItem(
      "context",
      JSON.stringify({
        userId: 0,
        planName: "",
      })
    );
  return JSON.parse(localStorage.getItem("context"));
};

export default function useStateContext() {
  const { context, setContext } = useContext(stateContext);
  return {
    context,
    setContext: (obj) => {
      setContext({ ...context, ...obj });
    },
  };
}

export function ContextProvider({ children }) {
  const [context, setContext] = useState(getNewContext());

  useEffect(() => {
    localStorage.setItem("context", JSON.stringify(context));
  }, [context]);

  return (
    <stateContext.Provider value={{ context, setContext }}>
      {children}
    </stateContext.Provider>
  );
}
