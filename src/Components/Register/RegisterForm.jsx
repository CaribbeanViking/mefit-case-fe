import React from "react";
import { useState } from "react";
import { createAPIEndpoint } from "../../API";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";

export default function RegisterForm() {
  const [field, setField] = useState({
    email: "",
    password: "",
  });
  const [confirmedPassword, setConfirmedPassword] = useState("");
  const [error, setError] = useState("");
  const navigate = useNavigate("");
  useEffect(() => {
    async function makeRequest() {
      await delay(500);
    }
    makeRequest();
  });

  const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms)); // Set delay function

  const handleChange = (e) => {
    const { id, value } = e.target;
    setField((prevField) => ({
      ...prevField,
      [id]: value,
    }));
  };

  const handleConfirmedPasswordChange = (e) => {
    setConfirmedPassword(e.target.value); // Use state for local password confirm
  };

  const handleBackToLogin = () => {
    navigate("/"); // Navigate to login
  };

  const handleSubmitClick = async (e) => {
    e.preventDefault();

    if (validateEmail()) {
      if (validatePassword()) {
        if (field.password === confirmedPassword) {
          if (await userUnique()) {
            setError("New user has signed up!");
            await delay(1000);
            sendToEndpoint();
            navigate("/");
          } else {
            setError("User already exists!");
            await delay(1000);
            setField({
              // Clear fields
              email: "",
              password: "",
            });
            setConfirmedPassword("");
          }
        } else {
          setError("Passwords don't match!");
          await delay(1000);
          setField({
            email: field.email,
            password: "",
          });
        }
      } else {
        await delay(1000);
        setField({
          email: field.email,
          password: "",
        });
        setConfirmedPassword("");
      }
    } else {
      await delay(1000);
      setField({
        email: "",
        password: "",
      });
      setConfirmedPassword("");
    }
  };

  const validateEmail = () => {
    let temp = {};
    temp.email = /\S+@\S+\.\S+/.test(field.email); // Test inputed wmail structure
    if (field.email === "") {
      // If no input
      setError("Input Email address");
      return false;
    }
    if (!temp.email) {
      // If not valid email address
      setError("Invalid Email address");
      return false;
    }
    return true;
  };

  const validatePassword = () => {
    let temp = {};
    temp.password =
      /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/.test(
        // Test inputed password
        field.password
      );
    if (field.password === "" || confirmedPassword === "") {
      // If empty password
      setError("Input password");
      return false;
    }
    if (!temp.password) {
      // If weak password
      setError("Pick a stronger password");
      return false;
    }
    return true;
  };

  const userUnique = async () => {
    // Test if user already exists
    var users = await getUsers();
    for (let data of users.data) {
      if (data.email === field.email) {
        return false;
      }
    }
    return true;
  };

  const getUsers = async () => {
    var users = await createAPIEndpoint("User").get(); // Get all users
    return users;
  };

  const sendToEndpoint = async () => {
    // Send information to endpoint
    var Id = 0;
    const userPayload = {
      // Set email and password
      Email: field.email,
      Password: field.password,
      Is_Admin: 0,
      Is_Contributor: 0,
    };
    await createAPIEndpoint("User").post(userPayload);

    var users = await getUsers();

    for (let data of users.data) {
      if (data.email === field.email) {
        Id = data.id; // Set id
      }
    }

    const addressPayload = {};
    await createAPIEndpoint("Address").post(addressPayload); // Set empty address

    const statsPayload = {
      // Set current workout to false
      Button_1: false,
      Button_2: false,
      Button_3: false,
    };
    await createAPIEndpoint("Stats").post(statsPayload);

    const accountPayload = {
      // Set all IDs to same except for plan
      UserID: Id,
      AddressID: Id,
      StatsID: Id,
      PlanID: null,
    };
    await createAPIEndpoint("Account").post(accountPayload);
  };

  return (
    <div className="register-box">
      <h1 className="register-title">MeFit</h1>
      <h2 className="register-text">Create new account</h2>
      <form className="register-form">
        <fieldset className="register-fieldset">
          <label htmlFor="username"></label>
          <div className="innerRegisterBox">
            <input
              className="register-input"
              type="text"
              id="email"
              onChange={handleChange}
              value={field.email || ""}
              name="username"
              autoComplete="off"
              placeholder="Email"
            />
            <input
              className="register-input"
              type="password"
              id="password"
              onChange={handleChange}
              value={field.password || ""}
              name="password"
              autoComplete="off"
              placeholder="Select Password"
            />
            <input
              className="register-input"
              type="password"
              id="retypePassword"
              onChange={handleConfirmedPasswordChange}
              value={confirmedPassword || ""}
              name="retype-password"
              autoComplete="off"
              placeholder="Repeat Password"
            />
          </div>
        </fieldset>
        <div className="error-container-reg">
          <p className="register-error">{error}</p>
        </div>
        <div className="button-container">
          <button
            type="submit"
            className="button back-button"
            onClick={handleBackToLogin}
          >
            <p className="button-text">Back to Login</p>
          </button>
          <button
            type="submit"
            className="button reg-button"
            onClick={handleSubmitClick}
          >
            <p className="button-text">Sign up</p>
          </button>
        </div>
      </form>
    </div>
  );
}
