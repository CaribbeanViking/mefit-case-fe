import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./Views/Login";
import Register from "./Views/Register";
import Profile from "./Views/Profile";
import Landing from "./Views/Landing";
import Goals from "./Views/Goals";
import Programs from "./Views/Programs";
import ChangePassword from "./Views/ChangePassword";
import { WithNav } from "./Components/Navbar/WithNav";
import { WithOutNav } from "./Components/Navbar/WithOutNav";
import Admin from "./Views/Admin";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route element={<WithOutNav />}>
            <Route path="/" element={<Login />} />
            <Route path="/Register" element={<Register />} />
          </Route>
          <Route element={<WithNav />}>
            <Route path="/Admin" element={<Admin />} />
            <Route path="/Landing" element={<Landing />} />
            <Route path="/Profile" element={<Profile />} />
            <Route path="/ChangePassword" element={<ChangePassword />} />
            <Route path="/Goals" element={<Goals />} />
            <Route path="/Programs" element={<Programs />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;
