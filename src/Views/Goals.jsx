import CurrentGoalForm from "../Components/CurrentGoal/CurrentGoalForm";
import "./Goals.css";
import { useNavigate } from "react-router-dom";
import useStateContext from "../Components/Login/Hooks/useStateContext";
import { useEffect } from "react";
export default function Goals() {
  const navigate = useNavigate("")
  const {context} = useStateContext()
  useEffect(() =>{
    if(context.userId > 0){

    }else{
     navigate("/")
    }
    
 })
  return (
    <div className="goals">
      <div>
        <CurrentGoalForm />
      </div>
    </div>
  );
}
