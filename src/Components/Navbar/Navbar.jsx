import React from "react";
import "./Navbar.css";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import useStateContext from "../Login/Hooks/useStateContext";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRightFromBracket,
  faIdCard,
  faHouse,
  faDumbbell,
} from "@fortawesome/free-solid-svg-icons";

const Navbar = () => {
  const navigate = useNavigate("");
  const [homeIsActive, setHomeIsActive] = useState(false);
  const [profileIsActive, setProfileIsActive] = useState(false);
  const [isGoalActive, setGoalIsActive] = useState(false);
  const { context } = useStateContext();
  const handleLogoutClick = () => {
    context.userId = 0;

    localStorage.removeItem("context");
    navigate("/");
  };

  const handleProfileClick = () => {
    setProfileIsActive(true);
    setHomeIsActive(false);
    setGoalIsActive(false);
    navigate("/Profile");
  };

  const handleHomeClick = () => {
    setProfileIsActive(false);
    setHomeIsActive(true);
    setGoalIsActive(false);
    navigate("/Landing");
  };

  const handleGoalClick = () => {
    setProfileIsActive(false);
    setHomeIsActive(false);
    setGoalIsActive(true);
    navigate("/Goals");
  };

  return (
    <div>
      <nav className="v-center">
        <div className="nav-item">
          <button
            className="nav-button home-button"
            style={{
              backgroundColor: homeIsActive ? "rgb(250, 250, 251)" : "",
              color: homeIsActive ? "rgb(52, 178, 51)" : "",
            }}
            onClick={handleHomeClick}
          >
            <FontAwesomeIcon icon={faHouse} className="icon-size" />
          </button>
        </div>
        <div className="nav-item">
          <button
            className="nav-button goal-button"
            style={{
              backgroundColor: isGoalActive ? "rgb(250, 250, 251)" : "",
              color: isGoalActive ? "rgb(52, 178, 51)" : "",
            }}
            onClick={handleGoalClick}
          >
            <FontAwesomeIcon icon={faDumbbell} className="icon-size" />
          </button>
        </div>
        <h1 className="profile-title">MeFit</h1>
        <div className="nav-item">
          <button
            className="nav-button profile-button"
            style={{
              backgroundColor: profileIsActive ? "rgb(250, 250, 251)" : "",
              color: profileIsActive ? "rgb(52, 178, 51)" : "",
            }}
            onClick={handleProfileClick}
          >
            <FontAwesomeIcon icon={faIdCard} className="icon-size" />
          </button>
        </div>
        <div className="nav-item">
          <button
            className="nav-button logout-button"
            onClick={handleLogoutClick}
          >
            <FontAwesomeIcon
              icon={faArrowRightFromBracket}
              className="icon-size"
            />
          </button>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
