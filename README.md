## Using MeFit

This project was created using the React Framework to launch the website, type in "npm start" into a terminal. Then MeFit will launch using your standard browser on port 3000.

## Installation
Type "npm install" into a command prompt to install all the dependencies, as well as .

## Project Description

MeFit is an application used to document and track workouts, to reach a specific fitness goal. It includes user functionality, with admin control.

# Using the Application

Login/Signup Page: Is presented once you first log into the page, where you can sign up to the website or log in to an existing account.
Navgation Bar: Has 4 links, these are the landing page, Goals page, Profile page and Sign up page.
Landing page: Has a dashboard component which shows the current goal and the current date and time. You can also set a new goal on this page.
Current Goal Page: Here is where you can view the current goal and update it upon a completed workout, and if all workouts are completed you can set a new goal.    
Profile Page: Displays all user information, you can also update it.
Admin Page: Can only be accessed when you are logged in as Admin, this is a page where you can delete users from the website.

### Contributors
https://gitlab.com/CaribbeanViking
https://gitlab.com/saintgod
https://gitlab.com/oskar.nyman


