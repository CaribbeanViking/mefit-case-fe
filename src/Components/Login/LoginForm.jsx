import React from "react";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { createAPIEndpoint } from "../../API";
import { useEffect } from "react";
import useStateContext from "./Hooks/useStateContext";

export default function LoginForm() {
  // Context shared with whole app
  const { setContext } = useStateContext();
  const navigate = useNavigate("");
  useEffect(() => {
    async function makeRequest() {
      await delay(500);
    }
    makeRequest();
  });

  const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms)); // Delay function
  const [field, setField] = useState({
    email: "",
    password: "",
  });

  const [error, setError] = useState("");

  const handleChange = (e) => {
    const { id, value } = e.target;
    setField((prevField) => ({
      ...prevField,
      [id]: value,
    }));
  };

  const handleLoginClick = async (e) => {
    e.preventDefault();
    var user = await getUser(); // Get current user
    var planId = await getPlan(user.id); // Get the active plan for current user
    if (user !== null) {
      // If user doesn't exist
      if (await checkPassword(user.password)) {
        // If password is correct
        setContext({ userId: user.id, planId: planId }); // Set the context to current ids
        setError("Logging in..");
        await delay(500);
        login();
      } else if (field.password === "") {
        // If no password
        setError("Input password");
        await delay(1000);
      } else {
        setError("Incorrect password"); // If incorrect password
        await delay(1000);
        setField({
          email: field.email,
          password: "",
        });
      }
    } else if (field.email === "") {
      // If no email
      setError("Input Email address");
      await delay(1000);
    } else {
      setError("No such email exists");
      await delay(1000);
      setField({
        email: "",
        password: "",
      });
    }
  };

  const getUser = async () => {
    // Find current user
    var users = await createAPIEndpoint("User").get();
    for (let data of users.data) {
      if (data.email === field.email) {
        return data;
      }
    }
    return null;
  };

  const getAccount = async (userId) => {
    // Find current account
    return await createAPIEndpoint("Account").getById(userId);
  };

  const getPlan = async (userId) => {
    // Get planID for current user
    var account = await getAccount(userId);
    var planId = Object.values(account.data)[8];
    return planId;
  };

  const checkPassword = async (password) => {
    //Compare password to database password
    if (field.password === password) {
      return true;
    }
    return false;
  };

  const login = async () => {
    await delay(500);
    navigate("/Landing");
  };

  return (
    <div>
      <div className="login-box">
        <h1 className="login-title">MeFit</h1>
        <h2 className="login-text">Login to your account</h2>

        <form className="login-form">
          <fieldset className="login-fieldset">
            <label htmlFor="username"></label>
            <div className="innerLoginBox">
              <p className="member-text">
                Not a member? Sign up{" "}
                <a href="/Register" className="register-link">
                  here.
                </a>
              </p>
              <input
                className="login-input"
                type="text"
                autoComplete="off"
                placeholder="Email"
                id="email"
                onChange={handleChange}
                value={field.email || ""}
                name="username"
              />
              <input
                className="login-input"
                type="password"
                autoComplete="off"
                placeholder="Password"
                id="password"
                onChange={handleChange}
                value={field.password || ""}
                name="password"
              />
            </div>
          </fieldset>
          <div className="error-container">
            <p className="login-error">{error}</p>
          </div>

          <div className="button-container">
            <button
              type="submit"
              className="button login-button register-button-text"
              onClick={handleLoginClick}
            >
              <p className="button-text">Login</p>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
