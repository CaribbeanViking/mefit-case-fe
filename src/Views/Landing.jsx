import Dashboard from "../Components/Landing/Dashboard/Dashboard";
import "./Landing.css";
import { useNavigate } from "react-router-dom";
import useStateContext from "../Components/Login/Hooks/useStateContext";
import { useEffect } from "react";
export default function Landing() {
  const navigate = useNavigate("");
  const { context } = useStateContext();
  useEffect(() => {
    if (context.userId > 0) {
    } else {
      navigate("/");
    }
  });
  return (
    <div className="landing">
      <Dashboard />
    </div>
  );
}
