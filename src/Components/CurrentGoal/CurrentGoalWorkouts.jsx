import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { createAPIEndpoint } from "../../API";
import useStateContext from "../Login/Hooks/useStateContext";

// Handles the selection of workouts and button pressing. Takes in the programId and sends out the workoutId to the parent component.
export default function CurrentGoalWorkouts({ programId, setWorkoutId }) {
  const [workouts, setWorkouts] = useState([]);
  const { context, setContext } = useStateContext();

  const [btn1IsActive, setBtn1IsActive] = useState(false);
  const [btn2IsActive, setBtn2IsActive] = useState(false);
  const [btn3IsActive, setBtn3IsActive] = useState(false);
  const [compPlans, setCompPlans] = useState([]);

  // Gets all the workouts based on programId provided by the parent component 
  useEffect(() => {
    createAPIEndpoint("Plan/" + programId + "/workouts")
      .get()
      .then((response) => {
        let workoutList = response.data.map((item) => ({
          id: item.id,
          name: item.name,
          description: item.description,
        }));

        setWorkouts(workoutList);
        // sends out an array of workoutIds that can be used to load in the exercises related to these in a sibling component
        let temp = [];
        response.data.forEach((element) => {
          temp.push(element.id);
        });
        setWorkoutId(temp);
        getCurrentStats();
      });
  }, []);

  // Gets the Stats and then specifies the buttons in each if statement
  const getCurrentStats = async () => {
    var stats = await createAPIEndpoint("Stats").getById(context.userId);
    if (stats.data.button_1 === true) {
      setBtn1IsActive(true);
    }
    if (stats.data.button_2 === true) {
      setBtn2IsActive(true);
    }
    return stats;
  };

  // Button handling and updates the database with a bool value
  const onClickFirst = async () => {
    if (btn2IsActive !== true) {
      setBtn1IsActive(true);
      const payload = {
        Id: context.userId,
        Button_1: true,
      };
      await createAPIEndpoint("Stats").put(context.userId, payload);
    }
  };
  const onClickSecond = async () => {
    if (btn1IsActive === true) {
      setBtn2IsActive(true);
      const payload = {
        Id: context.userId,
        Button_1: true,
        Button_2: true,
      };
      await createAPIEndpoint("Stats").put(context.userId, payload);
    }
  };
  const onClickThird = async () => {
    var stats = await getCurrentStats();
    if (btn2IsActive === true) {
      setBtn3IsActive(true);
      var list = stats.data.plans;
      list.push(context.planId);

      // When all the buttons are pushed the stats are reset.
      await createAPIEndpoint("Stats").putStatsReset(context.userId + "/reset");

      // Adds the latest planId to the Stats list of plans
      await createAPIEndpoint("Stats")
        .PutPlanId(context.userId + "/plans/" + context.planId)

        .then((response) => {
          setCompPlans(response.data);
        });
      setContext({ planId: 0 });
      console.log(list);
    }
  };

  // Sets up button handling and colour schemes
  return (
    <div>
      <div className="current-goal-buttons">
        <button
          className="workout-button-curr"
          onClick={onClickFirst}
          style={{
            backgroundColor: btn1IsActive ? "rgb(52, 178, 51)" : "",
          }}
        >
          <h1>{}</h1>
        </button>
        <button
          className="workout-button-curr"
          onClick={onClickSecond}
          style={{
            backgroundColor: btn2IsActive ? "rgb(52, 178, 51)" : "",
          }}
        >
          <h1>{}</h1>
        </button>
        <button
          className="workout-button-curr"
          onClick={onClickThird}
          style={{
            backgroundColor: btn3IsActive ? "rgb(52, 178, 51)" : "",
          }}
        >
          <h1>{}</h1>
        </button>
      </div>
    </div>
  );
}
