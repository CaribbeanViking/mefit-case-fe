import React from "react";
import { useNavigate } from "react-router-dom";
import { createAPIEndpoint } from "../../API";
import useStateContext from "../Login/Hooks/useStateContext";

// Component that will set and update the database with the selected ProgramId derived from the parent.
export default function ProgramSelectButton({ programId }) {
  const { setContext } = useStateContext();
  const { context } = useStateContext();

  // prepares for a navigation to another page with a short delay
  const navigate = useNavigate("");
  const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

  var planIdOnEnter = context.planId;

  // Updates the Account in the database with the selected programId
  const onClick = async () => {
    await createAPIEndpoint("Account").PutPlanId(
      context.userId + "/updatePlanId/" + programId,
      programId
    );

    // Resets all the bools in the Stats as this is dependant on the current program selected
    await createAPIEndpoint("Stats").putStatsReset(context.userId + "/reset");
    setContext({ planId: parseInt(programId) });
    await delay(500);

    if (planIdOnEnter === 0) {
      navigate("/Goals");
    } else {
      navigate("/Landing");
    }
  };

  return (
    <div className="selectButton-box">
      <button onClick={onClick} className="selectButton-button">
        Confirm Program
      </button>
    </div>
  );
}
