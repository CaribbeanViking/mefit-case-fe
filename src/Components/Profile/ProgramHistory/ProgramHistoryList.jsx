import React, { useEffect } from "react";
import { useState } from "react";
import { createAPIEndpoint } from "../../../API";
import useStateContext from "../../Login/Hooks/useStateContext";

export default function ProgramHistoryList() {
  const [compPlansId, setCompPlansId] = useState([]);
  const [existingPlans, setExistingPlans] = useState([]);
  const { context } = useStateContext();

  useEffect(() => {
    // Get info on page load
    getInfo();
  }, []);

  const getInfo = async () => {
    var plans = await createAPIEndpoint("Plan").get();
    var stats = await createAPIEndpoint("Stats").getById(context.userId);
    // Set completed plans and existing plans
    setCompPlansId(Object.values(stats.data)[5]);
    setExistingPlans(Object.values(plans.data));
  };

  const getExistingPlanNames = () => {
    // Get all names for the plans in database
    let names = [];
    for (let i = 0; i < existingPlans.length; i++) {
      names[i] = existingPlans[i].name;
    }
    return names;
  };

  const makeList = () => {
    // Create list of completed plans
    const existingPlanNames = getExistingPlanNames();
    let list = [];
    for (let i = 0; i < compPlansId.length; i++) {
      for (let j = 0; j < existingPlanNames.length; j++) {
        if (compPlansId[i] === j) {
          list[i] = existingPlanNames[j - 1];
        }
      }
    }
    return list;
  };

  const arr = makeList();

  return (
    <div>
      <ul className="user-info-list">
        {arr.map((e, i) => (
          <div key={i}>
            <li className="user-info-list-item-input">{e} Program</li>
          </div>
        ))}
      </ul>
    </div>
  );
}
