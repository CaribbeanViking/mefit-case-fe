import { useEffect } from "react";
import { useState } from "react";
import { createAPIEndpoint } from "../../API";

// Gets the program by programId supplied by the parent
export default function GoalsProgram({ programId }) {
  const [select, setSelected] = useState("");
  const [plan, setCurrentPlan] = useState([]);

  // Gets the plan by programId and saves the data
  useEffect(() => {
    createAPIEndpoint("Plan")
      .getById(programId)
      .then((response) => {
        setCurrentPlan(response.data);
        setSelected(response.data.id);
      })
      .catch((err) => console.log(err));
  }, [programId]);

  // Presents the plan name saves in the useState
  return (
    <div className="program-list-box-curr">
      <h1>{plan.name} Program</h1>
    </div>
  );
}
