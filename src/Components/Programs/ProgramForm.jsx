import { useState } from "react";
import ProgramList from "./ProgramList";
import ProgramSelectButton from "./ProgramSelectButton";
import WorkoutList from "./WorkoutList";

// Parent Component that will house ProgramList, WorkoutList and ProgramSelectionButton components
// Using data passing between children and parent in order to keep track of certain events and Ids
const ProgramForm = () => {
  // used to store programId (planId)
  const [programId, setProgramId] = useState(0);

  return (
    <div className="program-form">
      <div className="program-form-box">
        <div>
          <ProgramList setProgramId={setProgramId} />
        </div>
        <div>
          <WorkoutList programId={programId} />
        </div>
        <div className="btn-container">
          <ProgramSelectButton programId={programId} />
        </div>
      </div>
    </div>
  );
};
export default ProgramForm;
