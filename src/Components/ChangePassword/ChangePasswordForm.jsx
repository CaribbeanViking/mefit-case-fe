import React from "react";
import { useState, useEffect } from "react";
import { createAPIEndpoint } from "../../API";
import useStateContext from "../Login/Hooks/useStateContext";
import { useNavigate } from "react-router-dom";

export default function ChangePasswordForm() {
  const navigate = useNavigate("");
  const { context } = useStateContext();
  const [field, setField] = useState({
    newPassword: "",
    newRetypedPassword: "",
    oldPassword: "",
  });
  const [error, setError] = useState("");

  useEffect(() => {
    async function makeRequest() {
      await delay(500);
    }
    makeRequest();
  });

  const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

  const handleChange = (e) => {
    const { id, value } = e.target;
    setField((prevField) => ({
      ...prevField,
      [id]: value,
    }));
  };

  var userId = context.userId;

  const getUserById = async () => {
    const user = await createAPIEndpoint("User").getById(userId);
    return user;
  };

  var oldUserPassword = "";
  const getUserPassword = async () => {
    // Get old password
    var user = await getUserById();
    oldUserPassword = user.data.password;
  };
  getUserPassword();

  var userData;
  const getUserData = async () => {
    var user = await getUserById();
    userData = user.data;
  };
  getUserData();

  const handleSubmitClick = async (e) => {
    e.preventDefault();
    if (validateOldPassword()) {
      // Check if old password is correct
      if (validateNewPasswords()) {
        // Check if new password is strong
        if (compareNewPasswords()) {
          // Check of new passwords match
          if (newPasswordIsUnique()) {
            // Check if new password is same as old
            setError("Password changed!");
            await delay(1000);
            setNewPassword();
            navigate("/Profile");
            await delay(1000);
          } else {
            await delay(1000);
            resetFields();
          }
        } else {
          await delay(1000);
          resetFields();
        }
      } else {
        await delay(1000);
        resetFields();
      }
    } else {
      await delay(1000);
      resetFields();
    }
  };

  const setNewPassword = async () => {
    // Set new password
    const userPayload = {
      Id: userId,
      First_Name: Object.values(userData)[1],
      Last_Name: Object.values(userData)[2],
      Email: Object.values(userData)[3],
      Password: field.newPassword,
      Is_Contributor: Object.values(userData)[5],
      Is_Admin: Object.values(userData)[6],
    };
    await createAPIEndpoint("User").put(userId, userPayload);
  };

  const validateOldPassword = () => {
    if (
      // If any field is empty
      field.newPassword === "" ||
      field.oldPassword === "" ||
      field.newRetypedPassword === ""
    ) {
      setError("All fields are required.");
      return false;
    }
    if (field.oldPassword === oldUserPassword) {
      // Compare old password
      return true;
    } else {
      setError("Old password is wrong.");
      return false;
    }
  };

  const validateNewPasswords = () => {
    let tempNew = {};
    let tempNewRetyped = {};

    tempNew.newPassword =
      /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/.test(
        // Test new password
        field.newPassword
      );
    tempNewRetyped.newRetypedPassword =
      /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/.test(
        field.newPassword
      );
    if (!tempNew.newPassword || !tempNewRetyped.newRetypedPassword) {
      setError("Pick a stronger password.");
      return false;
    } else {
      return true;
    }
  };

  const compareNewPasswords = () => {
    if (field.newPassword === field.newRetypedPassword) {
      return true;
    }
    setError("New passwords don't match.");
    return false;
  };

  const newPasswordIsUnique = () => {
    if (field.newPassword !== oldUserPassword) {
      return true;
    }
    setError("New password cannot be same as old.");
    return false;
  };

  const resetFields = () => {
    setField({
      newPassword: "",
      newRetypedPassword: "",
      oldPassword: "",
    });
  };

  return (
    <div className="change-password-box">
      <h3 className="change-password-title">Change Password</h3>
      <form className="change-password-form">
        <fieldset className="change-password-fieldset">
          <label htmlFor="username"></label>
          <div className="innerPasswordBox">
            <input
              className="change-password-input"
              type="password"
              id="newPassword"
              onChange={handleChange}
              value={field.newPassword || ""}
              name="password"
              autoComplete="off"
              placeholder="Select New Password"
            />
            <input
              className="change-password-input"
              type="password"
              id="newRetypedPassword"
              onChange={handleChange}
              value={field.newRetypedPassword || ""}
              name="retype-password"
              autoComplete="off"
              placeholder="Repeat Password"
            />
            <input
              className="change-password-input"
              type="password"
              id="oldPassword"
              onChange={handleChange}
              value={field.oldPassword || ""}
              name="password"
              autoComplete="off"
              placeholder="Input Old Password"
            />
          </div>
        </fieldset>
        <div className="error-container">
          <p className="change-error">{error}</p>
        </div>
        <div className="change-pass-button-container">
          <button
            type="submit"
            className="change-pass-button"
            onClick={handleSubmitClick}
          >
            <p className="button-text">Apply</p>
          </button>
        </div>
      </form>
    </div>
  );
}
