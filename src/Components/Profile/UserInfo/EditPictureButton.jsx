import React from "react";

export default function EditPictureButton() {
  return <button className="edit-pic-button">Edit</button>;
}
