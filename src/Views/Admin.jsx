import React from "react";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import AdminPage from "../Components/Admin/AdminPage";
import useStateContext from "../Components/Login/Hooks/useStateContext";
import "./Admin.css";

const Admin = () => {
  const navigate = useNavigate("");
  const { context } = useStateContext();
  useEffect(() => {
    if (context.userId > 0) {
    } else {
      navigate("/");
    }
  });
  return (
    <div className="admin">
      <AdminPage />
    </div>
  );
};

export default Admin;
