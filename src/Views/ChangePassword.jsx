import "./ChangePassword.css";
import ChangePasswordForm from "../Components/ChangePassword/ChangePasswordForm";

export default function ChangePassword() {
  return (
    <div className="change-password">
      <ChangePasswordForm />
    </div>
  );
}
