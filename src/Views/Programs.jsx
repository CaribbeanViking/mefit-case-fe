import ProgramForm from "../Components/Programs/ProgramForm";

import "./Programs.css";
import { useNavigate } from "react-router-dom";
import useStateContext from "../Components/Login/Hooks/useStateContext";
import { useEffect } from "react";
export default function Programs() {
  const navigate = useNavigate("")
  const {context} = useStateContext()
  useEffect(() =>{
    if(context.userId > 0){

    }else{
     navigate("/")
    }
    
 })
  return (
    <div className="programs">
      <ProgramForm />
    </div>
  );
}
