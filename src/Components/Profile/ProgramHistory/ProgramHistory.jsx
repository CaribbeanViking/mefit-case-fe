import React from "react";
import useStateContext from "../../Login/Hooks/useStateContext";
import ProgramHistoryList from "./ProgramHistoryList";

export default function ProgramHistory() {
  // Context shared with all app
  const { context } = useStateContext();

  return (
    <div className="program-history-container">
      <div className="user-info-header"></div>
      <div className="history-box">
        <div className="user-history-title-container">
          <h3 className="user-info-title">Completed programs</h3>
        </div>

        <div className="innerHistoryBox">
          <ProgramHistoryList programId={context.planId} />
        </div>
      </div>
    </div>
  );
}
