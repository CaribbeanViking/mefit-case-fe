import "./Profile.css";

import ProgramHistory from "../Components/Profile/ProgramHistory/ProgramHistory";
import UserInfo from "../Components/Profile/UserInfo/UserInfo";
import { useNavigate } from "react-router-dom";
import useStateContext from "../Components/Login/Hooks/useStateContext";
import { useEffect } from "react";
export default function Profile() {
  const navigate = useNavigate("");
  const { context } = useStateContext();
  useEffect(() => {
    if (context.userId > 0) {
    } else {
      navigate("/");
    }
  });
  return (
    <div className="profile">
      <UserInfo />
      <ProgramHistory />
    </div>
  );
}
