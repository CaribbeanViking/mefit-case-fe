import React from "react";
import { useNavigate } from "react-router-dom";

export default function ChangePasswordButton() {
  const navigate = useNavigate("");
  const handleClick = (e) => {
    e.preventDefault();
    navigate("/ChangePassword");
  };
  return (
    <button className="change-password-button" onClick={handleClick}>
      Change Password
    </button>
  );
}
