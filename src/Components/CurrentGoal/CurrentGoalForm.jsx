import { useState } from "react";
import { useNavigate } from "react-router-dom";

import useStateContext from "../Login/Hooks/useStateContext";
import CurrentGoalExercises from "./CurrentGoalExercise";
import GoalsProgram from "./CurrentGoalProgram";
import CurrentGoalWorkouts from "./CurrentGoalWorkouts";

// Parent component that will handle several component children: GoalsProgram, CurrentGoalWorkouts, CurrentGoalExercises
const CurrentGoalForm = () => {
  const [workoutId, setWorkoutId] = useState([]);
  const { context } = useStateContext();
  const navigate = useNavigate();

  // handles navigation on program selection button
  const handleClick = () => {
    navigate("/Programs");
  };

  if (context.planId !== 0) {
    return (
      <div className="current-goal-form-box">
        <div className="goal-plan-name">
          <GoalsProgram programId={context.planId} />
        </div>
        <div>
          <CurrentGoalWorkouts
            programId={context.planId}
            setWorkoutId={setWorkoutId}
          />
        </div>
        <div>
          <ul className="workout-ul-container">
            {workoutId.map((element, index) => (
              <ul key={index} className="exercise-ul-container">
                <li className="exercise-li-container">
                  <CurrentGoalExercises element={element} />
                </li>
              </ul>
            ))}
          </ul>
        </div>
      </div>
    );
  } else {
    return (
      <div className="no-plan-box">
        <h1 className="">You have no current goal!</h1>
        <h3>Hit the button to set up a new goal.</h3>
        <div className="btn-container">
          <button className="create-goal-button" onClick={handleClick}>
            New goal
          </button>
        </div>
      </div>
    );
  }
};

export default CurrentGoalForm;
