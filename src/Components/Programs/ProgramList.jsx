import { useEffect, useState } from "react";
import { createAPIEndpoint } from "../../API";

// Passing programId up to the parent after the correct programId has been selected.
export default function ProgramList({ setProgramId }) {
  const [select, setSelected] = useState("");
  const [programList, setProgramList] = useState([]);

  // Gets all the programs(plans) and inputs to a list
  useEffect(() => {
    createAPIEndpoint("Plan")
      .get()
      .then((response) => {
        let programList = response.data.map((item) => ({
          id: item.id,
          name: item.name,
        }));
        // Sets default value to the list and then updates the useState
        programList = [{ id: 0, name: "Select Program" }].concat(programList);
        setProgramList(programList);
      })
      .catch((err) => console.log(err));
  }, []);

  // Upon a change the programId is set to the changed value which is then sent up to the parent
  const handleChange = (e) => {
    const temp = e.target.value;
    setProgramId(temp);
    setSelected(e.target.value); // event - input field (target) - value of input field
  };

  // Select box/list is created and the programList is mapped to each item new item.
  return (
    <div className="program-list-box">
      <label>
        <select
          className="select-program"
          label="Program"
          name="programId"
          value={select}
          onChange={handleChange}
        >
          {programList.map((item) => (
            <option key={item.name} value={item.id}>
              {item.name}
            </option>
          ))}
        </select>
      </label>
    </div>
  );
}
