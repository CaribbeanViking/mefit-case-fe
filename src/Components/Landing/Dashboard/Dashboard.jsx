import React, { useEffect, useState } from "react";
import "./Dashboard.css";
import { useNavigate } from "react-router-dom";
import useStateContext from "../../Login/Hooks/useStateContext";
import { createAPIEndpoint } from "../../../API";
import DashboardCurrentGoal from "./DashboardCurrentGoal";

export default function Dashboard() {
  const { context } = useStateContext();
  const [current_day, setCurrentDay] = useState("");
  const [current_month, setCurrentMonth] = useState("");
  const [current_year, setCurrentYear] = useState("");
  const [currentPlanName, setCurrentPlanName] = useState("");
  const [currentPlanDescr, setCurrentPlanDescr] = useState("");
  const navigate = useNavigate("");

  function setCurrentMonthToString(month) {
    var monthtoString = "";
    switch (month) {
      case 0:
        monthtoString = "January";
        break;
      case 1:
        monthtoString = "February";
        break;
      case 2:
        monthtoString = "March";
        break;
      case 3:
        monthtoString = "April";
        break;
      case 4:
        monthtoString = "May";
        break;
      case 5:
        monthtoString = "June";
        break;
      case 6:
        monthtoString = "July";
        break;
      case 7:
        monthtoString = "August";
        break;
      case 8:
        monthtoString = "September";
        break;
      case 9:
        monthtoString = "October";
        break;
      case 10:
        monthtoString = "November";
        break;
      case 11:
        monthtoString = "December";
        break;
      default:
        break;
    }
    return monthtoString;
  }

  useEffect(() => {
    createAPIEndpoint("Plan")
      .getById(context.planId)
      .then((response) => {
        setCurrentPlanName(response.data.name);
        setCurrentPlanDescr(response.data.description);
      })
      .catch((err) => console.log(err));

    var dateObj = new Date();

    setCurrentDay(dateObj.getUTCDate());

    setCurrentMonth(setCurrentMonthToString(dateObj.getUTCMonth()));

    setCurrentYear(dateObj.getUTCFullYear());
  }, []);

  const handleSetGoalClick = () => {
    navigate("/Programs");
  };

  return (
    <div className="outer-page">
      <h1>Dashboard</h1>
      <div className="outer-container">
        <div className="container">
          <div className="day">
            <ul>
              <li className="calender-out">
                <div className="month-out">{current_month}</div>
                <div className="day-out">{current_day}</div>
                <div className="year-out">{current_year}</div>
              </li>
            </ul>
          </div>
          <div className="goal-container">
            <div className="goal-of-the-day">
              <h1>Current Goal</h1>
            </div>

            <div className="program-name">
              <h2>{currentPlanName.toUpperCase()}</h2>
            </div>

            <div className="program-descr">
              <h3 className="plan-descr-text">{currentPlanDescr}</h3>
            </div>
            <DashboardCurrentGoal />

            <div className="btn-container">
              <button
                className="create-goal-button"
                onClick={handleSetGoalClick}
              >
                Set a new goal!
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
