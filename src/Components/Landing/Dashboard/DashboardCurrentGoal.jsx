import React, { useState } from "react";
//import "./Dashboard/DashboardCurrentGoal.css"
import useStateContext from "../../Login/Hooks/useStateContext";
import { useEffect } from "react";
import { createAPIEndpoint } from "../../../API";
const DashboardCurrentGoal = () => {
  const { context } = useStateContext();
  const [stats, setStats] = useState(0);
  const [numberToString, setNumberString] = useState("NOT LOADED DB");
  useEffect(() => {
    createAPIEndpoint("Stats")
      .getById(context.userId)
      .then((res) => {
        let count = 0;
        let data = res.data;
        if (data.button_1 === false) {
          count++;
        }
        if (data.button_2 === false) {
          count++;
        }
        if (data.button_3 === false) {
          count++;
        }
        switch (count) {
          case 1:
            setNumberString("ONE");
            break;
          case 2:
            setNumberString("TWO");
            break;
          case 3:
            setNumberString("THREE");
            break;
          default:
            break;
        }
        setStats(count);
      });
  }, []);

  if (context.planId !== 0) {
    return (
      <div>
        <div className="workouts-left-box">
          <div className="workouts-left-text workouts-left-text-upper">
            <br />
            YOU HAVE
          </div>
          <div className="workouts-left-text workouts-left-text-mid">
            {stats}
          </div>
          <div className="workouts-left-text workouts-left-text-lower">
            WORKOUTS LEFT
          </div>
        </div>
      </div>
    );
  } else
    return (
      <div>
        <div className="workouts-left-box">
          <div className="workouts-left-text workouts-left-text-upper">
            <br />
          </div>

          <p className="select-goal-dash">You have not selected a goal!</p>

          <div className="workouts-left-text workouts-left-text-lower"></div>
        </div>
      </div>
    );
};

export default DashboardCurrentGoal;
