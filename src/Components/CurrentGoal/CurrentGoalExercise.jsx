import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { createAPIEndpoint } from "../../API";

// Handles the exercises and what should be shown depending on the workoutId (element) submitted by the sibling component
export default function CurrentGoalExercises({ element }) {
  const [exercises, setExercises] = useState([]);

  // Gets the exercises associated to the specified workoutId (element)
  useEffect(() => {
    // workoutId.forEach(element => {
    createAPIEndpoint("Workout/" + element + "/exercises")
      .get()
      .then((response) => {
        let workoutList = response.data.map((item) => ({
          id: item.id,
          name: item.name,
          description: item.description,
        }));

        setExercises(workoutList);
      });
  }, [element]);

  // Maps the exercise list with a ul to present both exercise name and description
  return (
    <div>
      <div>
        {exercises.map((item, index) => (
          <ul key={index} className="exercise-box-curr">
            <h3 className="workout-name-curr">{item.name}</h3>
            <li className="exercise-li">
              <em>{item.description}</em>
            </li>
          </ul>
        ))}
      </div>
    </div>
  );
}
