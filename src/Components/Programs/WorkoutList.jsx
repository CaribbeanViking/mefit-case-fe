import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { createAPIEndpoint } from "../../API";

// Manages the list of workouts associated with each programId
export default function WorkoutList({ programId }) {
  const [workoutList, setWorkoutList] = useState([]);

  // Gets the workouts based on the programId submitted by the parent component
  useEffect(() => {
    createAPIEndpoint("Plan/" + programId + "/workouts")
      .get()
      .then((response) => {
        let workoutList = response.data.map((item) => ({
          id: item.id,
          name: item.name,
          description: item.description,
        }));
        setWorkoutList(workoutList);
      })
      .catch((err) => console.log(err));
  }, [programId]);

  // Maps the workout-list to a new header with an underlying description
  return (
    <div className="workout-box">
      <ul className="wrk-ul">
        {workoutList.map((item, index) => (
          <ul key={index} className="wrk-li">
            <h2 className="wrk-name">{item.name}</h2>
            <li>
              <p className="descr-name">{item.description}</p>
            </li>
          </ul>
        ))}
      </ul>
    </div>
  );
}
