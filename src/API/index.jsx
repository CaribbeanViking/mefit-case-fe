import axios from "axios";

// connection url to the API
export const BASE_URL = "https://localhost:7266/";

export const createAPIEndpoint = (endpoint) => {
  let url = BASE_URL + "api/" + endpoint + "/";

  return {
    get: () => axios.get(url + "all"),
    getById: (id) => axios.get(url + id),
    post: (newRecord) => axios.post(url, newRecord),
    put: (id, updatedRecord) => axios.put(url + id, updatedRecord),
    // Updates a certain Button value(in api) by ID
    putBool: (id, ButtonID) => axios.put(url + id, ButtonID),
    // Puts a plan id with a PlanID argument
    PutPlanId: (id, PlanID) => axios.put(url + id, PlanID),
    // resets all the bools in a Stats
    putStatsReset: (id) => axios.put(url + id),
    delete: (id) => axios.delete(url + id),
  };
};
