import LoginForm from "../Components/Login/LoginForm";
import "./Login.css";

export default function Login() {
  return (
    <div className="login">
      <LoginForm />
    </div>
  );
}
