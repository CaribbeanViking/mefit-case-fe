import "./Register.css";
import React from "react";
import RegisterForm from "../Components/Register/RegisterForm";

export default function Register() {
  return (
    <div className="register">
      <RegisterForm />
    </div>
  );
}
