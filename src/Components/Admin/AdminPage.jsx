import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { createAPIEndpoint } from "../../API";
import { useNavigate } from "react-router-dom";
import useStateContext from "../Login/Hooks/useStateContext";

const AdminPage = () => {
  const [users, setUsers] = useState([]);
  const navigate = useNavigate();
  const { context } = useStateContext();

  if (context.userId !== 1) {
    navigate("/Profile");
  }

  const updateUser = () => {
    createAPIEndpoint("User")
      .get()
      .then((res) => {
        let userList = res.data.map((user) => ({
          Id: user.id,
          FirstName: user.first_Name,
          Email: user.email,
        }));
        setUsers(userList);
      });
  };
  const deleteUser = async (e) => {
    e.preventDefault();
    if (users.length !== 1) {
      await createAPIEndpoint("Account").delete(parseInt(e.target.value));
      await createAPIEndpoint("User").delete(parseInt(e.target.value));
      await createAPIEndpoint("Address").delete(parseInt(e.target.value));
      await createAPIEndpoint("Stats").delete(parseInt(e.target.value));
    } else {
      alert("Cant delete all users!");
    }
    updateUser();
  };
  useEffect(() => {
    updateUser();
  }, []);
  return (
    <div className="admin-page">
      <h1>Admin Page</h1>
      <div className="admin-box">
        <div className="user-page-title">
          List of Currently Registered Users
        </div>
        <hr className="hr-reg"></hr>
        <ul className="user-list-admin">
          <li>
            {users.length !== 0 &&
              users.map((user, key) => (
                <div className="registered-user">
                  <button
                    id="user"
                    className="user-reg-btn"
                    value={user.Id}
                    onClick={deleteUser}
                  >
                    Delete
                  </button>
                  <div className="email-reg">Email: {user.Email}</div>
                </div>
              ))}
          </li>
        </ul>
      </div>
    </div>
  );
};

export default AdminPage;
