import React from "react";
import { useNavigate } from "react-router-dom";
import useStateContext from "../../Login/Hooks/useStateContext";

export default function AdminControlButton() {
  const { context } = useStateContext();
  var userId = context.userId;
  const navigate = useNavigate("");

  const handleClick = async (e) => {
    e.preventDefault();
    if (userId === 1) navigate("/Admin");
    else alert("You don't have Admin privileges.");
  };
  return (
    <button className="contributor-request-button" onClick={handleClick}>
      Admin
    </button>
  );
}
